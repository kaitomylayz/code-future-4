# Работа шаблонов
from django import template

# Шаблонный фильтры
from django.template.defaultfilters import stringfilter

# Регистрация собственных шаблонов тега
register = template.Library()


# --------------------------------- Теги --------------------------------------

# Установка стиля для активной ссылки (выбранной страницы)
@register.simple_tag
def header_active_link_class():
    return "nav-link px-2 link-secondary"


# Установка стиля для пассивной ссылки (не выбранной страницы)
@register.simple_tag()
def header_passive_link_class():
    return "nav-link px-2"


# Использования условия для выбора активной или пассивной ссылки (Текущая страница, Ожидаемая страница)
# Если название не хочется использовать в качестве вывода тега
# можно называть тег с помощью свойства name, тогда для вызова надо будет прописать имя, а не функцию
@register.simple_tag(name='head_link')
def header_link(curr_page, state):
    if curr_page == state:
        return header_active_link_class()
    return header_passive_link_class()


# ------------------------------------- Фильтры ------------------------------

# Фильтр добавления денежного знака
# В фильтре в начале должно быть value - значение переменной, т.к. фильтры используются с перменными
@register.filter()
def money_tag(value, currency_unit: str = "RUB"):
    value = str(value)
    if currency_unit == "RUB":
        return value + ' ₽'
    elif currency_unit == "USD":
        return value + ' $'
    elif currency_unit == "EUR":
        return value + ' €'
    else:
        return value


# Фильтр ожидания строк
# @stringfilter

# ---------------------------- Теги включения ----------------------------
# Теги включения - это теги с заготовленными шаблонами
# Сначала сделаем простой тег для вывода тегов,
@register.simple_tag()
def show_tags(item):
    tags = item.tag.all()
    return {"tags": tags}


# Теперь создадим тег включения и добавим в него шаблон
@register.inclusion_tag("magazine/_inc/_tags.html")
def show_tags_inclusion(item):
    tags = item.tag.all()
    return {"tags": tags}

