from django.test import TestCase
from magazine.utils import *


class CalculateMoneyDefTestCase(TestCase):
    def test_sum_count_price_pass(self):
        result = sum_price_count(price=100, count=10)
        self.assertEqual(1000, result)

    def test_sum_count_price_discount_pass(self):
        result = sum_price_count(price=200, count=15, discount=5)
        self.assertEqual(2850, result)

    def test_sum_count_price_nds_pass(self):
        result = sum_price_count(price=400, count=6, nds=7)
        self.assertEqual(2232, result)

    def test_sum_count_price_discount_nds_pass(self):
        result = sum_price_count(price=500, count=105, discount=6, nds=12)
        self.assertEqual(43428, result)


class CalculateMoneyClassTestCase(TestCase, CalculateMoney):
    def test_sum_price_pass(self):
        list_price = [294, 2000, 6942]
        result = self.sum_price(list_price)
        self.assertEqual(9236, result)

    def test_sum_price_count_pass(self):
        result = self.sum_price_count(price=400, count=19)
        self.assertEqual(7600, result)
