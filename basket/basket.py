from decimal import Decimal
from django.conf import settings
from magazine.models import Product

# {
#     'basket': {
#         '5': {
#           'quantity': 2,
#           'price': 284,
#           'product':object,
#           'total_price':8372
#         }
#     }
#     ''
# }


class Basket:
    # Создание корзины
    def __init__(self, request):
        self.session = request.session  # Берем сессию
        basket = self.session.get(settings.BASKET_SESSION_ID)  # По ключу забираем данные из сессии
        if not basket:  # Если объекта корзины нет, то создаём с ключом указаным в настройках, и/или сохраняем в переменную
            basket = self.session[settings.BASKET_SESSION_ID] = {}
        self.basket = basket

    def __iter__(self):
        # Перебор товаров в корзине и получения объектов из БД
        product_ids = self.basket.keys()  # Получение ID из ключей словаря

        product_list = Product.objects.filter(pk__in=product_ids)

        basket = self.basket.copy()
        for product in product_list:
            basket[str(product.pk)]['product'] = product

        for item in basket.values():
            item['total_price'] = float(item['price']) * int(item['quantity'])
            yield item

    def __len__(self):
        return sum(item['quantity'] for item in self.basket.values())

    def save(self):
        # Обновление сессии basket
        self.session[settings.BASKET_SESSION_ID] = self.basket
        # Отметить сеанс как "измененный", чтобы убедиться, что он сохранен
        self.session.modified = True

    def add(self, product, quantity=1, update_quantity=False):
        product_id = str(product.id)
        if product_id not in self.basket:
            self.basket[product_id] = {'quantity': 0,
                                     'price': str(product.price)}
        if update_quantity:
            self.basket[product_id]['quantity'] = quantity
        else:
            self.basket[product_id]['quantity'] += quantity
        self.save()

    def remove(self,product):
        product_id = str(product.id)
        if product_id in self.basket:
            del self.basket[product_id]
            self.save()

    def get_total_price(self):
        return sum(float(item['price']) * int(item['quantity']) for item in self.basket.values())

    def clear(self):
        del self.session[settings.BASKET_SESSION_ID]
        self.session.modified = True
        